'use strict';

var methods = require('./methods');
var restler = require('./restler');

module.exports = function (config) {
    var transport = restler(config);
    return methods(transport, config);
};
