/* jshint browser: true */

'use strict';

var _ = {
    defaultsDeep: require('lodash.defaultsdeep')
};

var debug = function () {
    var args =  [].slice.call(arguments);
    var msg = '%c> stackbin:sdk ' + args.shift();
    args.unshift('color: #449933');
    args.unshift(msg);
    console.log.apply(console, args);
};

// This is for 429s only
var BACKOFF = 1000;

//const MAX_BACKOFF = 1000 ^ 5;

// This is for timeout only
var MAX_RETRIES = 5;
var TIMEOUT = 30000;

// REST DEFAULTS
var DEFAULTS = {
    timeout: TIMEOUT
};

function rest(method, resource, payload, config) {

    var backoff = BACKOFF;
    var auth_attempted = false;
    var retries = 0;

    payload = payload && JSON.stringify(payload);

    return new Promise(function (resolve, reject) {

        var err;
        var url = config.protocol + '://' + config.api_host;

        method = method.toUpperCase();

        //debug('Making REST call', method, url, resource);
        debug('%s %s/%s (Payload: %s, Token: %s)', method, url, resource, (payload || '<empty>'), (config.token ? '...'+config.token.substr(-7) :  '<empty>'));

        var request = new XMLHttpRequest();
        request.withCredentials = true;

        request.open(method, url + '/' + resource);

        request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
        request.setRequestHeader('X-API-Version', config.version);

        if (config.token) {
            //debug('Using token', '...' + config.token.substr(-7));
            request.setRequestHeader('Authorization', 'Bearer ' + config.token);
        }

        request.onreadystatechange = function () {

            if (request.readyState === 4) {

                var json;
                try {
                    // request.responseText might be empty (for example, in response to a HEAD)
                    json = request.responseText && JSON.parse(request.responseText);

                } catch (err) {

                    var nerr = new Error('Error communicating with API');
                    nerr.name = 'StackbinTransportError';
                    nerr.statusCode = request.status;
                    nerr.debug = {
                        response: request.responseText
                    };

                    reject(nerr);
                    return;
                }

                if (!json) {
                    json = {
                        error: {
                            message: 'Unknown Error',
                            type: 'UnknownError',
                            debug: {
                                reason: 'Response JSON is falsy'
                            }
                        }
                    };
                }

                switch (request.status) {
                    case 200:
                        resolve(json);
                        break;

                    case 204:
                        if (method.toUpperCase() === 'DELETE') {
                            resolve();
                        } else {
                            err = new Error('Unexpected 204');
                            err.name = 'StackbinTransportError';
                            err.statusCode = request.status;
                            reject(err);
                        }

                        break;

                    // bad request
                    case 400:
                        err = new Error(json.error.message);
                        err.name = 'Stackbin' + json.error.type;
                        err.statusCode = request.status;
                        reject(err);
                        break;

                    case 404:
                        err = new Error('Resource Not Found');
                        err.name = 'StackbinResourceNotFound';
                        err.statusCode = request.status;
                        reject(err);
                        break;

                    // not logged in, or similar authentication error
                    case 401:

                        err = new Error(json.error.message);
                        err.name = 'StackbinTransportAuthenticationError';
                        err.statusCode = request.status;
                        reject(err);

                        // if (!auth_attempted) { //&& !rest_config.token && config.username && config.password) {
                        //     debug('Auth not yet attempted, and token is empty');
                        //
                        //     reject(new Error('Feature Disabled'));
                        //
                        //     // auth_attempted = true;
                        //     //
                        //     // rest('post', 'tokens', {}, config)
                        //     //     .then(function (data) {
                        //     //         debug('Success! Retrying with token...', data);
                        //     //         config.token = data.token;
                        //     //         rest_config.headers.Authorization = 'Bearer ' + config.token;
                        //     //         request.send(payload);
                        //     //     })
                        //     //     .then(resolve)
                        //     //     .catch(reject);
                        //
                        // } else {
                        //     err = new Error(json.error.message);
                        //     err.name = 'StackbinTransportAuthenticationError';
                        //     err.statusCode = request.status;
                        //     reject(err);
                        // }

                        break;

                    // not allowed to do that!
                    case 403:
                        reject(new Error('Permission Denied', {statusCode: request.status}));
                        break;

                    // throttled!
                    case 429:
                        reject(new Error('Requests are throttled. Try later', {statusCode: request.status}));

                        // if (backoff < MAX_BACKOFF) {
                        //     backoff *= 2;
                        //     debug('Retrying in ' + backoff);
                        //     retry(backoff);
                        // } else {
                        //     err.name = 'TransportCapacityError';
                        //     reject(err);
                        // }
                        break;

                    // crap yourself by default
                    default:
                        err = new Error(json.error && json.error.message || 'Transport Error ' + request.status);
                        err.name = 'Stackbin' + (json.error && json.error.type || 'TransportGeneralError');
                        err.statusCode = request.status;
                        reject(err);
                        break;

                }
            }
        };

        request.ontimeout = function () {
            if (++retries < MAX_RETRIES) {
                debug('Retrying in ' + TIMEOUT);
                this.retry(TIMEOUT);
            } else {
                err = new Error('Timeout during postJson  of ' + url);
                err.name = 'StackbinTransportTimeoutError';
                reject(err);
            }
        };

        request.send(payload);

    });

}

var transport = function transport(config) {

    config = _.defaultsDeep(config, {
        protocol: 'https'
    });

    return {
        head: function (path) {
            return rest('head', path, null, config);
        },

        get: function (path) {
            return rest('get', path, null, config);
        },

        put: function (path, payload) {
            return rest('put', path, payload, config);
        },

        post: function (path, payload) {
            return rest('post', path, payload, config);
        },

        patch: function (path, payload) {
            return rest('patch', path, payload, config);
        },

        del: function (path) {
            return rest('delete', path, null, config);
        }
    };

};

module.exports = transport;
