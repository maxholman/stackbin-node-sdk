require('es-nodeify');

var _ = {
    defaultsDeep: require('lodash.defaultsdeep')
};

const DEFAULTS = {
    version: 'latest',
    api_host: 'api.stkbn.com'/*,
    region: 'ap-southeast-1'*/
};

module.exports = function (transport, config) {

    var sdk = {

        /**
         * @param {Object} config
         * @returns {Promise}
         */
        config: function sdk_config(config) {
            config = _.defaultsDeep(config, DEFAULTS);
            return this; //chaining
        },

        /**
         * @param {String} token
         * @returns {Promise}
         */
        setToken: function (token) {
            config.token = token;
            return this; //chaining
        },

        /**`
         * @param {Function} [cb]
         * @returns {Promise}
         */
        listAccounts: function (cb) {
            return transport.get('accounts').nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {String} [params.account_id]
         * @param {String} [params.domain]
         * @param {Function} [cb]
         * @returns {Promise}
         */
        getAccount: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain);
            return transport.get(path).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {String} [params.account_id]
         * @param {String} [params.domain]
         * @param {Function} [cb]
         * @returns {Promise}
         */
        addAccount: function (params, cb) {
            var path = 'accounts';
            return transport.post(path, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {String} [params.account_id]
         * @param {String} [params.domain]
         * @param {Function} [cb]
         * @returns {Promise}
         */
        updateAccount: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain);
            return transport.put(path, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {String} [params.account_id]
         * @param {String} [params.domain]
         * @param {Function} [cb]
         * @returns {Promise}
         */
        deleteAccount: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain);
            return transport.del(path, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {String} [params.domain]
         * @param {Function} [cb]
         * @returns {Promise}
         */
        domainAvailable: function (params, cb) {
            var path = 'accounts/' + params.domain;
            return transport.head(path).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {String} params.project_id
         * @param {Function} [cb]
         * @returns {Promise}
         */
        getProject: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/projects/' + (params.project_id || params.slug);
            return transport.get(path).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {String} params.account_id
         * @param {Function} [cb]
         * @returns {Promise}
         */
        listProjects: function (params, cb) {
            return transport.get('accounts/' + (params.account_id || params.domain) + '/projects').nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        getReport: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/projects/' + (params.project_id || params.slug) + '/reports/' + params.report_id;
            return transport.get(path).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        listReports: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/projects/' + (params.project_id || params.slug) + '/reports';
            return transport.get(path).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        updateReport: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/projects/' + (params.project_id || params.slug) + '/reports/' + params.report_id;
            return transport.put(path, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        addReport: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/projects/' + (params.project_id || params.slug) + '/reports';
            return transport.post(path, params).nodeify(cb);
        },

        /**
         * @name createToken
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        createToken: function (params, cb) {
            return transport.post('tokens', params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        addUser: function (params, cb) {
            return transport.post('users', params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {String} [params.user_id=me]
         * @param {Function} [cb]
         * @returns {Promise}
         */
        getUser: function (params, cb) {
            var user_id = params && params.user_id || 'me';
            return transport.get('users/' + user_id).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {String} [params.user_id=me]
         * @param {Function} [cb]
         * @returns {Promise}
         */
        updateUser: function (params, cb) {
            var user_id = params && params.user_id || 'me';
            return transport.put('users/' + user_id, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        addAuth: function (params, cb) {
            return transport.post('users/' + params.user_id + '/auths', params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        deleteAuth: function (params, cb) {
            return transport.del('users/' + params.user_id + '/auths/' + params.auth_id, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        listAuths: function (params, cb) {
            return transport.get('users/' + params.user_id + '/auths', params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        updateInvite: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/invites/' + params.invite_id;
            delete params.domain;
            delete params.invite_id;
            return transport.put(path, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {String} params.domain
         * @param {String} params.invite_id
         * @param {Function} [cb]
         * @returns {Promise}
         */
        getInvite: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/invites/' + params.invite_id;
            return transport.get(path, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        addInvite: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/invites';
            delete params.domain;
            return transport.post(path, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {String} params.domain
         * @param {Function} [cb]
         * @returns {Promise}
         */
        listInvites: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/invites';
            return transport.get(path, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        deleteInvite: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/invites/' + params.invite_id;
            return transport.del(path, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        addMember: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/members';
            delete params.domain;
            return transport.post(path, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        deleteMember: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/members/' + params.user_id;
            delete params.domain;
            return transport.del(path, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        listMembers: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/members';
            return transport.get(path).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        getSource: function (params, cb) {
            var path = 'sources/' + params.source_id;
            return transport.get(path).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        updateSource: function (params, cb) {
            var path = 'sources/' + params.source_id;
            return transport.put(path, params).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {String} params.account_id
         * @param {String} params.project_id
         * @param {String} params.ingress_id
         * @param {Function} [cb]
         * @returns {Promise}
         */
        getIngress: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/projects/' + (params.project_id || params.slug) + '/ingress/' + params.ingress_id;
            return transport.get(path).nodeify(cb);
        },

        /**
         * @param {Object} params
         * @param {String} params.account_id
         * @param {String} params.project_id
         * @param {String} params.ingress_id
         * @param {Function} [cb]
         * @returns {Promise}
         */
        delIngress: function (params, cb) {
            var path = 'accounts/' + (params.account_id || params.domain) + '/projects/' + (params.project_id || params.slug) + '/ingress/' + params.ingress_id;
            return transport.del(path).nodeify(cb);
        }
    };

    sdk.config(config || {});

    return sdk;

};
