'use strict';

var methods = require('./methods');
var xhr = require('./xhr');

module.exports = function (config) {
    var transport = xhr(config);
    return methods(transport, config);
};
