'use strict';

var restler = require('restler');
var debug = require('debug')('stackbin:sdk:restler');
var manifest = require('../package.json');

var _ = {
    defaultsDeep: require('lodash.defaultsdeep')
};

// This is for 429s only
const BACKOFF = 1000;
const MAX_BACKOFF = 1000 ^ 5;

// This is for timeout only
const MAX_RETRIES = 3;
const TIMEOUT = 30000;

// REST DEFAULTS
var DEFAULTS = {
    timeout: TIMEOUT,
    headers: {
        'User-Agent': manifest.name + '/' + manifest.version
    }
};

if (process.env.npm_package_name) {
    DEFAULTS.headers['User-Agent'] = process.env.npm_package_name + '/' + (process.env.npm_package_version || '0.0') + ' (' + DEFAULTS.headers['User-Agent'] + ')';
}

function rest(method, resource, payload, config) {

    var backoff = BACKOFF;
    var auth_attempted = false;
    var retries = 0;

    return new Promise(function (resolve, reject) {

        var host = `${config.protocol}://${config.api_host}`;
        var url = `${host}/${resource}`;

        var rest_config = _.defaultsDeep({
            headers: {},
            timeout: config.timeout
        }, DEFAULTS);

        if (config.token) {
            debug('Using token', '...' + config.token.substr(-7));
            rest_config.headers.Authorization = 'Bearer ' + config.token;
        }

        debug('Using config', rest_config);

        //debug('Making REST call', method, url, resource);
        debug('%s %s', method, url);
        debug('Payload', payload || '<no payload>');

        var restler_method;
        if (method === 'get' || method === 'del') {
            payload = rest_config;
            rest_config = undefined;
            restler_method = method;
        } else {
            restler_method = method + 'Json';
        }

        restler[restler_method](url, payload, rest_config)
            .on('success', function (data) {
                debug('Gotcha!', data);
                backoff = BACKOFF;
                resolve(data);
            })
            .on('fail', function (data, response) {

                debug('Fail Event. Response:', response.statusCode, data);

                var retry = this.retry.bind(this);

                var err;

                switch (response.statusCode) {
                    case 400:
                        err = new Error(data.error.message);
                        err.name = 'Stackbin' + data.error.type;
                        err.statusCode = response.statusCode;
                        reject(err);
                        break;

                    case 401:
                        if (!auth_attempted && !rest_config.token && config.username && config.password) {
                            debug('Auth not yet attempted, and token is empty');
                            auth_attempted = true;

                            restler.post(`${host}/tokens`, _.defaultsDeep({}, config, DEFAULTS))
                                .on('success', function (data, response) {
                                    debug('Success! Retrying with token...', data);
                                    config.token = data.token;
                                    rest_config.headers.Authorization = 'Bearer ' + config.token;
                                    retry(0);
                                })
                                .on('fail', function (data, response) {
                                    debug('Fail! ', data);
                                    err = new Error(`${data.error.message} (${data.error.type})`);
                                    err.statusCode = response.statusCode;
                                    err.name = 'StackbinTransportAuthenticationError';
                                    reject(err);
                                })
                                .on('error', function (err) {
                                    debug('Error! ', err);
                                    err.name = 'StackbinTransportHttpError';
                                    err.message += ' whilst fetching ' + url;
                                    reject(err);
                                });

                        } else {
                            err = new Error(data.error.message);
                            err.name = 'StackbinTransportAuthenticationError';
                            err.statusCode = response.statusCode;
                            reject(err);
                        }

                        break;

                    case 429:

                        if (backoff < MAX_BACKOFF) {
                            backoff *= 2;
                            debug('Retrying in ' + backoff);
                            retry(backoff);
                        } else {
                            err.name = 'TransportCapacityError';
                            reject(err);
                        }

                        break;

                    case 404:
                        err = new Error('Resource Not Found ' + url);
                        err.name = 'StackbinResourceNotFound';
                        err.statusCode = response.statusCode;
                        reject(err);
                        break;

                    default:
                        err = new Error(data.error && data.error.message || 'Transport Error ' + response.statusCode);
                        err.name = 'Stackbin' + (data.error && data.error.type || 'TransportGeneralError');
                        err.statusCode = response.statusCode;
                        reject(err);
                        break;
                }

            })
            .on('error', function (err, response) {
                err.name = 'StackbinTransportHttpError';
                err.message += ' whilst fetching ' + url;
                reject(err);
            })
            .on('timeout', function () {

                if (++retries < MAX_RETRIES) {
                    debug('Retrying in ' + config.timeout);
                    this.retry(config.timeout);
                } else {
                    var err = new Error('Timeout during postJson on ' + url);
                    err.name = 'StackbinTransportTimeoutError';
                    reject(err);
                }

            });
    });

}

module.exports = function transport(config) {

    config = _.defaultsDeep(config, {
        protocol: 'https'
    });

    return {
        head: function (path) {
            return rest('head', path, null, config);
        },

        get: function (path) {
            return rest('get', path, null, config);
        },

        put: function (path, payload) {
            return rest('put', path, payload, config);
        },

        post: function (path, payload) {
            return rest('post', path, payload, config);
        },

        patch: function (path, payload) {
            return rest('patch', path, payload, config);
        },

        del: function (path) {
            return rest('del', path, null, config);
        }
    };

};
